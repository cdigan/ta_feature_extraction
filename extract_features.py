'''
@Author: Conor digan

@Date: 8th August 2017
'''

import sys
from datetime import datetime, date, time
import numpy as np
import pandas as pd


def multiple_days(dates):
	'''
	See if the user logged in on multiple different dates
    '''
	val = True if len(set(dates)) > 1 else False
	return val


def is_weekday_biz(timestamp, weekdays = (1,2,3,4,5), day_start = time(9,0,0), day_end = time(17,0,0)):
	'''
	Check if an individual timestamp is on a weekday and during business hours.
	Weekdays and business hours can be changed as arguments
	'''
	dow = timestamp.dayofweek
	t = timestamp.time()

	val = False
	if dow in weekdays and t >= day_start and t <= day_end:
		val = True
	return val


def avg_daily_logs(dates):
	'''
	Get average number of times a user logged on per day
	'''
	min_date = min(dates)
	max_date = max(dates)
	num_days = (max_date - min_date).days+1
	avg_logs = len(dates)/num_days
	return avg_logs



def engineer_user_features(df):

	mult_days = multiple_days(df['date'])
	wk_biz = sum(df['weekday_biz'] == True)*2 > df.shape[0]
	avg_logins = avg_daily_logs(df['date'])
	return pd.Series([mult_days, wk_biz, avg_logins])



#Check the command line arguments and parse them
assert len(sys.argv) == 3
logfile = sys.argv[1]
outfile = sys.argv[2]

print('... Reading the log data at ' + logfile)
#Read the logs into a pandas dataframe
logs = pd.read_csv(logfile)

print('... Extracting features')
#Convert the ts variable to datetime and extract date, time, dow & weekday_biz
logs['ts'] = pd.to_datetime(logs['ts'])
logs['date'] = logs['ts'].dt.date
logs['time'] = logs['ts'].dt.time
logs['dow'] = logs['ts'].dt.dayofweek
logs['weekday_biz'] = logs['ts'].apply(is_weekday_biz)

#Group the log files by uuid and extract the desired features
user_features = logs.groupby('uuid').apply(engineer_user_features).reset_index()
user_features.columns = ['uuid', 'multiple_days', 'weekday_biz', 'avg_daily_logs']


print('... Saving features to ' + outfile)
user_features.to_csv(outfile)
print('... Finished')




#print(logs.info())
#print('\n\n')
#print(logs.head())