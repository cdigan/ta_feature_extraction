# README #

This README gives a brief description of the methodologies I used for the 'Travel Audience Data Science Recruitment Challenge'

### multiple_days: ###

This feature is described as: 'Whether the user is active for multiple days'. To calculate this, I converted the list of all dates to a set to remove all duplicates. If the length of this set is greater than 1, than there are multiple distinct dates in the data.

### weekday_biz: ###

This feature is described as: 'Whether the user's traffic tends to occur during weekday business hours'. To calculate this, I first calculated whether each separate timestamp was during "weekday business hours". I set that to be Monday-Friday between 9:00 and 17:00. You can pass alternative days/times as your definition of weekday business hour as paramaters to "is_weekday_biz" function. Once that's been calculated for each timestamp, then I just needed to see if most of these are True or False. If most are True for a given user, then that user get's assigned True for weekday_biz

### avg_daily_logs: ###

This is my custom feature that calculates the average amount of times a user logs in per day. I took their time period as an 'active user' to be the number of days between when they first log on to the last day they log on.

### User Results: ###

There are 257353 distinct users. For multiple_days, 36035 are True, 221319 are False. For weekday_biz, 73222 are True, 184132 are False.